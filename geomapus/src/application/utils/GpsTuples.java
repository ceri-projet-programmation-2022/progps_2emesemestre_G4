package application.utils;

public class GpsTuples<X, Y> 
{
	private  double x;
	private  double y;
	
	public GpsTuples(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void setX(double newX)
	{
		this.x = newX;
	}
	

	public void setY(double newX)
	{
		this.y = newX;
	}
	
	public double getX()
	{
		return (double) x;
	}
	
	public double getY()
	{
		return (double) y;
	}

	@Override
	public String toString()
	{
		return "("+ x + "; " + y + ")";
	}
}

