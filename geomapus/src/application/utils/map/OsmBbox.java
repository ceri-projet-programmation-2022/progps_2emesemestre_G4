package application.utils.map;

import application.utils.GpsTuples;
//import javafx.util.Pair;

public class OsmBbox 
{
	private GpsTuples<Double, Double> osmOrigin;
	private GpsTuples<Double, Double> osmOposite;
	private GpsTuples<Double, Double> fxOrigin;
	
	/**
	 * bbox same as Overpass-turbo
	 * @param x1 	coordinate for bottom right corner
	 * @param y1	coordinate for bottom right corner
	 * @param x2	coordinate for up left corner
	 * @param y2	coordinate for up left corner
	 */
	public OsmBbox(double x1, double y1, double x2, double y2)
	{
		this.osmOrigin = new GpsTuples<Double, Double>(x1, y1);
		this.osmOposite = new GpsTuples<Double, Double>(x2, y2);
		this.fxOrigin = new GpsTuples<Double, Double>(x2, y1);
	}

	/**
	 * Get bottom right coordonate
	 * @return GpsTuples<Double, Double>
	 */
	public GpsTuples<Double, Double> getOsmOrigin() {
		return osmOrigin;
	}

	/**
	 * Get top left coordonate
	 * @return GpsTuples<Double, Double>
	 */
	public GpsTuples<Double, Double> getOsmOposite() {
		return osmOposite;
	}

	/**
	 * Get top right coordonate
	 * @return GpsTuples<Double, Double>
	 */
	public GpsTuples<Double, Double> getFxOrigin() {
		return fxOrigin;
	}
	
	
	@Override
	public String toString()
	{
		return "osmOrigin = " + osmOrigin + "\nosmOposite = " + osmOposite + "\nfxOrigin = " + fxOrigin;
	}
	
	/**
	 * Use it to build bbox for Overpass request
	 * @return String
	 */
	public String getStrOsm()
	{
		return "(" + this.getOsmOrigin().getX() + "," + this.getOsmOrigin().getY() + "," + this.getOsmOposite().getX() + "," + this.getOsmOposite().getY() + ")";
	}
	

	public void setOsmOrigin(double x, double y) {
		this.osmOrigin.setX(x);
		this.osmOrigin.setY(y);
		this.updateFxOrigin();
	}

	public void setOsmOposite(double x, double y) {
		this.osmOposite.setX(x);
		this.osmOposite.setY(y);
		this.updateFxOrigin();
	}

	/**
	 * Update fxOrigin
	 */
	private void updateFxOrigin()
	{
		this.fxOrigin.setX(this.osmOposite.getX());
		this.fxOrigin.setY(this.osmOrigin.getY());
	}
}
