package application.utils.map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * 
 * @author Aubertin Emmanuel (aka aTHO_)
 * @version 0.1
 * @since 0.1
 * 
 */
public class OsmMember {
	protected long id;
	protected String role;
	protected String type;
	
	/**
	 * Constructor, for more info about the attribute read Overpass API doc.
	 * @param 	id 
	 * @param	role
	 * @param	type
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public OsmMember(long id, String role, String type) {
		super();
		this.id 	= 	id;
		this.role 	= 	role;
		this.type 	= 	type;
	}

	/**
	 * Get the ref of the member
	 * @return ref
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public long getId() {
		return id;
	}

	/**
	 * Get the role of the member
	 * @return role
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getRole() {
		return role;
	}
	
	/**
	 * Get the role of the member
	 * @return role
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getType() {
		return type;
	}

	
}

