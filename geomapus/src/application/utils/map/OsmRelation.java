package application.utils.map;

import java.util.LinkedList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javafx.scene.Group;
import javafx.scene.shape.Polygon;


/**
 * 
 * @author Aubertin Emmanuel (aka aTHO_)
 * @version 0.1
 * @since 0.1
 * 
 */
public class OsmRelation {
	private long id;
	private LinkedList<OsmMember> memberList;
	private LinkedList<OsmTag> tagList;
	private OsmBounds bounds;
	
	
	/**
	 * Constructor with a relation Node and parentNode
	 * @param n relation Node of OpenStreetMap XML data file
	 * @param parentNode first node of Overpass responce
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public OsmRelation(Node n, Node parentNode) throws Exception {
		if( n.getNodeName() == "relation")
		{
			this.id 		= Long.parseLong(((Element) n).getAttribute("id"));
			this.memberList = new LinkedList<OsmMember>();
			this.tagList 	= new LinkedList<OsmTag>();
			if(n.hasChildNodes())
			{
				NodeList nodeList = n.getChildNodes();
				for(int i=1;  i < nodeList.getLength(); i++) 
				{
					Node currNode = nodeList.item(i);
					if( currNode.getNodeName() == "member")
					{
						
						this.addOsmMember(currNode, parentNode);
						continue;
					}
					if( currNode.getNodeName() == "tag")
					{
						this.addOsmTag(currNode);
						continue;
					}
					if( currNode.getNodeName() == "bounds")
					{
						this.bounds = new OsmBounds(currNode);
						continue;
					}
				}
			}
		} else {
			throw new Exception("Invalide Node ! Un node 'relation' étais attendu a la place de '" + ((Element) n).getTagName() +"'" );
		}
	}

	/**
	 * Add tag to the node
	 * @param n OSM tag Node
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
 	 */
	public void addOsmTag(Node n)
	{
		this.tagList.push(new OsmTag(n));
	}
	
	/**
	 * Add member to the node
	 * @param n OSM member Node
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
 	 */
	public void addOsmMember(Node n, Node parentNode)
	{
		if(((Element) n).getAttribute("type").equals("node"))
		{
			this.memberList.push(new OsmNode(Long.parseLong(((Element) n).getAttribute("ref")), ((Element) n).getAttribute("role"), parentNode));
			return;
		}
		if(((Element) n).getAttribute("type").equals("way"))
		{
			try {
				this.memberList.push(new OsmWay(Long.parseLong(((Element) n).getAttribute("ref")), ((Element) n).getAttribute("role"), parentNode));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
	}

	/**
	 * Get all OsmMember with a particular role
	 * @param role
	 * @return LinkedList<OsmMember> with member with the role
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmMember> getMemberType(String role)
	{
		LinkedList<OsmMember> outputList = new LinkedList<OsmMember>();
		System.out.println(this.memberList);
		for(int i=1; i < this.memberList.size(); i++)
		{
			OsmMember currMember = this.memberList.get(i);
			System.out.println("Current role = " + currMember.getRole());
			if(currMember.getRole().equals(role))
			{
				System.out.println("\tAdd outer !");
				outputList.push(currMember);
			}
		}
		return outputList;
	}
	
	
	/**
	 * Get all OsmWay from memberList with a particular role
	 * @param role
	 * @return LinkedList<OsmWay> with way from memberList
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmWay> getMemberTypeWay(String role)
	{
		LinkedList<OsmWay> outputList = new LinkedList<OsmWay>();
		System.out.println(this.memberList);
		for(int i=0; i < this.memberList.size(); i++)
		{
			OsmMember currMember = this.memberList.get(i);
			System.out.println("Current role = " + currMember.getRole());
			if(currMember.getType().equals("way") && currMember.getRole().equals(role))
			{
				outputList.push((OsmWay) currMember);
			}
		}
		return outputList;
	}
	
	
	public Group getPolygonOfRole(String role)
	{
		Group outGroup = new Group();
		LinkedList<OsmWay> wayList = this.getMemberTypeWay(role);
		Polygon outPoly = new Polygon();
		return new Group();
	}
	
	
	/**
	 * Get all OsmWay from memberList with a particular role
	 * @param role
	 * @return LinkedList<OsmNode> with Node from memberList
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmNode> getMemberTypeNode()
	{
		LinkedList<OsmNode> outputList = new LinkedList<OsmNode>();
		System.out.println(this.memberList);
		for(int i=1; i < this.memberList.size(); i++)
		{
			OsmMember currMember = this.memberList.get(i);
			System.out.println("Current role = " + currMember.getRole());
			if(currMember.getType().equals("node"))
			{
				outputList.push((OsmNode) currMember);
			}
		}
		return outputList;
	}
	
	/**
	 * Get the id of the member
	 * @return id
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public long getId() {
		return id;
	}

	/**
	 * Get member list of a relation 
	 * @return LinkedList<OsmMember>
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmMember> getMemberList() {
		return memberList;
	}

	/**
	 * Get tag list of a relation 
	 * @return LinkedList<OsmTag>
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmTag> getTagList() {
		return tagList;
	}
	
	/**
	 * Get the OsmRelation in a good string
	 * @return OsmRelation String
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String toString() {
		return Long.toString(this.id);
	}
	
	public OsmBounds getBounds() {
		return this.bounds;
	}
}
