package application.utils.map;

import java.util.LinkedList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import application.utils.GpsTuples;
import application.utils.map.OsmMember;
import application.utils.map.OsmNode;
import application.utils.map.OsmTag;


/**
 * 
 * @author Aubertin Emmanuel (aka aTHO_)
 * @version 0.1
 * @since 0.1
 * 
 */
public class OsmWay extends OsmMember {
	private LinkedList<OsmTag> tagList;
	private LinkedList<OsmNode> nodeList;
	
	/**
	 * Constructor for Way with role
	 * @param ref long number also call id
	 * @param role
	 * @param parentNode ParentNode of all xlm
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public OsmWay(long ref, String role, Node parentNode) throws Exception
	{
		
		super(ref, role, "way");
		
		if(parentNode.hasChildNodes()) {
			
			NodeList parentList = parentNode.getChildNodes();
			for(int i=1;  i < parentList.getLength(); i++) 
			{
				Node wayNode = parentList.item(i);
				if( wayNode.getNodeName() == "way" && Long.parseLong(((Element) wayNode).getAttribute("id")) == ref)
				{
						this.tagList 	= new LinkedList<OsmTag>();
						this.nodeList 	= new LinkedList<OsmNode>();
						if(wayNode.hasChildNodes())
						{
							NodeList nodeList = wayNode.getChildNodes();
							for(int j=1;  j < nodeList.getLength(); j++) 
							{
								Node currNode = nodeList.item(j);
								if( currNode.getNodeName() == "nd")
								{
									this.addOsmNd(currNode, parentNode);
									continue;
								}
								if( currNode.getNodeName() == "tag")
								{
									this.addOsmTag(currNode);
									continue;
								}
						}
					} else {
						throw new Exception("Invalide Node ! Un node 'way' étais attendu " );
					}
					break;
				}
			}
		}

	}
	
	/**
	 * Constructor for Way without role
	 * @param ref long number also call id
	 * @param parentNode ParentNode of all xlm
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public OsmWay(long ref, Node parentNode) throws Exception
	{
		
		super(ref, "null","way");
		
		if(parentNode.hasChildNodes()) {
			
			NodeList parentList = parentNode.getChildNodes();
			for(int i=1;  i < parentList.getLength(); i++) 
			{
				Node wayNode = parentList.item(i);
				if( wayNode.getNodeName() == "way" && Long.parseLong(((Element) wayNode).getAttribute("id")) == ref)
				{
					if(((Element) wayNode).getTagName() == "way") 
					{
						this.tagList 	= new LinkedList<OsmTag>();
						this.nodeList 	= new LinkedList<OsmNode>();
						if(wayNode.hasChildNodes()) {
							NodeList nodeList = wayNode.getChildNodes();
							for(int j=1;  j < nodeList.getLength(); j++) 
							{
								Node currNode = nodeList.item(j);
								if( currNode.getNodeName() == "nd")
								{
									this.addOsmNd(currNode, parentNode);
									continue;
								}
								if( currNode.getNodeName() == "tag")
								{
									this.addOsmTag(currNode);
									continue;
								}
							}
						}
					} else {
						throw new Exception("Invalide Node ! Un node 'way' étais attendu " );
					}
					break;
				}
			}
		}

	}
	
	
	/**
	 * Add nd to the node
	 * @param n OSM nd Node
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
 	 */
	public void addOsmNd(Node n, Node parentNode) 
	{
		this.nodeList.push(new OsmNode(Long.valueOf(((Element) n).getAttribute("ref")), parentNode));
	}
	
	/**
	 * Add tag to the node
	 * @param n OSM tag Node
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
 	 */
	public void addOsmTag(Node n)
	{
		this.tagList.push(new OsmTag(n));
	}

	/**
	 * Get the id of the member
	 * @return id
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public long getId() {
		return id;
	}

	/**
	 * Get List of all tag
	 * @return LinkedList<OsmTag>
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmTag> getTagList() {
		return tagList;
	}


	/**
	 * Get List of all tag
	 * @return LinkedList<OsmNode>
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmNode> getNodeList() {
		return nodeList;
	}
	
	/**
	 * Get the OsmWay in a good string
	 * @return OsmWay String
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String toString() 
	{
		return Long.toString(this.id);
	}
	

	
	/**
	 * Get distance btw 2 node
	 * @param n1
	 * @param n2
	 * @param zoom
	 * @return double
	 */
	static public double getdistance(OsmNode n1, OsmNode n2, int zoom)
	{
		return Math.sqrt(Math.pow((n2.getLat()*zoom-n1.getLat()*zoom), 2) + Math.pow((n2.getLon()*zoom-n1.getLon()*zoom), 2));
	}
	
	
	/**
	 * Get distance btw 2 point
	 * @param n1
	 * @param n2
	 * @return double
	 */
	static public double getdistance(double x1, double y1, double x2, double y2)
	{
		return Math.sqrt(Math.pow((x2-x1), 2) + Math.pow((y2-y1), 2));
	}
	
	/**
	 * Get distance btw 2 point
	 * @param n1
	 * @param n2
	 * @param zoom
	 * @return double
	 */
	static public double getdistance(GpsTuples<Double, Double> p1, GpsTuples<Double, Double> p2)
	{
		return Math.sqrt(Math.pow((p2.getX()-p1.getX()), 2) + Math.pow((p2.getY()-p1.getY()), 2));
	}
	
	/**
	 * Know if the Node get a particular tag
	 * @param value put the 'v' field
	 * @param key put the 'k' field
	 * @return true if the node has the tag
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public boolean hasTag(String key, String value) 
	{
		for (int i = 0; i < this.tagList.size(); i++)
		{
			if(this.tagList.get(i).getType().equals(key) && this.tagList.get(i).getValue().equals(value))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Know if the Node get a particular tag
	 * @param key put the 'k' field
	 * @return true if the node has the tag
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public boolean hasTag( String key ) 
	{
		for (int i = 0; i < this.tagList.size(); i++)
		{
			if(this.tagList.get(i).getType().equals(key) )
			{
				return true;
			}
		}
		return false;
	}
	
	public void fullPrint()
	{
		System.out.println("Way : " + this.id);
		System.out.println("Taglist :");
		for (int i = 0; i < this.tagList.size(); i++)
		{
			System.out.println("\t" + this.tagList.get(i));
		}
	}
	
	
	/**
	 * Know if the Node get a particular tag
	 * @param key put the 'k' field
	 * @return true if the node has the tag
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getTagValue( String key ) 
	{
		for (int i = 0; i < this.tagList.size(); i++)
		{
			if(this.tagList.get(i).getType().equals(key) )
			{
				return this.tagList.get(i).getValue();
			}
		}
		return null;
	}
}
