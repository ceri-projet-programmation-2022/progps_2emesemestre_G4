package application.utils.map;

import java.util.Collection;
import java.util.LinkedList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;


/**
 * 
 * @author Aubertin Emmanuel (aka aTHO_)
 * @version 0.1
 * @since 0.1
 * 
 */
public class OsmMap {
	private String generator;
	private String apiVersion;
	private LinkedList<OsmNode> nodeList;
	private LinkedList<OsmRelation> relationList;
	private LinkedList<OsmWay> wayList;
	private OsmBbox bbox;

	private double[] scaleVect;
	
	public double getScaleCoef()
	{
		return Math.max(this.scaleVect[0], this.scaleVect[1]);
	}
	
	public void setScaleVect(double x, double y)
	{
		System.out.println("this.scaleVect[0] = " + x + " | this.scaleVect[1] = " +y);
		scaleVect[0] = x;
		scaleVect[1] = y;
	}
	
	public OsmBbox getBbox() {
		return bbox;
	}


	public void setBbox(OsmBbox bbox) {
		this.bbox = bbox;
	}

	
	
	
	

	/**
	 * Constructor with the main Node
	 * @param n osm Node of OpenStreetMap XML data file
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public OsmMap(Node n) {
		super();
		this.scaleVect = new double[2];
		
		if(n.getNodeName() == "osm")
		{
			this.generator = ((Element) n).getAttribute("generator");
			this.apiVersion = ((Element) n).getAttribute("version");
		} else {
			this.generator = null;
			this.apiVersion = null;
		}
		this.nodeList 		= new LinkedList<OsmNode>();
		this.relationList 	= new LinkedList<OsmRelation>();
		this.wayList 		= new LinkedList<OsmWay>();
		
		if(n.hasChildNodes()) {
			NodeList nodeList = n.getChildNodes();
			for(int i=1; i < nodeList.getLength(); i++) {
				Node currNode = nodeList.item(i);
				if( currNode.getNodeName() == "relation")
				{
					this.addOsmRelation(currNode, n);
					continue;
				}
				if( currNode.getNodeName() == "way")
				{
					this.addOsmWay(currNode, n);
					continue;
				}
			}
			
		}
	}
	

	/**
	 * Add node to nodeList
	 * @param n node org.w3c.dom.Node to add
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public void addOsmNode(Node n, Node parentNode)
	{
		this.nodeList.push(new OsmNode(Long.parseLong(((Element) n).getAttribute("id")), parentNode));
	}
	
	/**
	 * Add relation to relationList
	 * @param n relation Node to add
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public void addOsmRelation(Node n, Node parentNode)
	{
		try {
			this.relationList.push(new OsmRelation(n, parentNode));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Add way to relationList
	 * @param n way Node to add
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public void addOsmWay(Node n, Node parentNode)
	{
		try {
			//System.out.println("ref : " + ((Element) n).getAttribute("id"));
			this.wayList.push(new OsmWay(Long.parseLong(((Element) n).getAttribute("id")), parentNode));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void displayRoad()
	{
		for(int i=0; i < this.wayList.size(); i++)
		{
			OsmWay currWay = this.wayList.get(i);
			LinkedList<OsmNode> currNodeList = currWay.getNodeList();
			System.out.println("------------");
			System.out.println("Size of wayList : " + this.getWayList().size());
			
			System.out.println("Size of getNodeList : " + currWay.getNodeList().size());
			System.out.println("Current NodeList: " + currNodeList);
			for(int j=0; j < currNodeList.size(); j++)
			{
				
				if(j < currNodeList.size()-1)
				{
					System.out.println("--");
					System.out.println("n1: " + currNodeList.get(j).getLat() + " | " + currNodeList.get(j).getLon());
					System.out.println("n2: " + currNodeList.get(j+1).getLat() + " | " + currNodeList.get(j+1).getLon());
					System.out.println("Distance: " + this.getdistance(currNodeList.get(j), currNodeList.get(j+1)));
				}
				
			}
		}	
	}
	
	/*
	 * 
	 * A ajouter plus tard pour la gestion des landuse multi polygon
	 * 
	 * public javafx.scene.Group getGroupLand()
	{
		LinkedList<OsmRelation> relList = this.getRelationList();
		Group outGroup = new Group();
		String fillColor = "#D4D3DC", strokeColor = "#D4D3DC";
		System.out.println("There is " + relList.size() + "Relation");
		for(int i=0; i < relList.size(); i++)
		{
			Group relGroup = new Group();
			OsmRelation currRel = relList.get(i);
			LinkedList<OsmMember> outerWay = currRel.getMemberType("way");
			
			for(int j=0; j < outerWay.size(); j++)
			{
				System.out.println("\tFirst outer !");

				OsmWay currWay = this.wayList.get(i);
				if(currWay.hasTag("landuse", "commercial"))
				{
					fillColor = "#E8AABE";
					 strokeColor= "#B4819B"; // 
				} else if(currWay.hasTag("landuse", "construstion"))
				{
					fillColor = "#AD9585";
					 strokeColor= "#796658"; // 
				}else if(currWay.hasTag("landuse", "industrial"))
				{
					fillColor = "#E8C2CE";
					 strokeColor= "#CCA3B0"; // 
				}
				else if(currWay.hasTag("landuse", "residenstial"))
				{
					fillColor = "#e5e7e6";
					 strokeColor= "#CFCECE";
				}
				else if(currWay.hasTag("landuse", "farmland"))
				{
					fillColor = "#e5e7e6";
					 strokeColor= "#CFCECE";
				}else if(currWay.hasTag("landuse", "forest"))
				{
					fillColor = "#e5e7e6";
					 strokeColor= "#CFCECE";
				}
				Polygon tempPolygon = new Polygon();
				LinkedList<OsmNode> currNodeList = currWay.getNodeList();
				for(int y=0; y < currNodeList.size(); y++)
				{
					
					Double[] currPoint = this.scalePoint(currNodeList.get(y).getLat(), currNodeList.get(y).getLon());
					System.out.println("x:" + currPoint[0] + " | y: " + currPoint[1]);
					tempPolygon.getPoints().addAll(currPoint);
				}
				tempPolygon.setStroke(Color.web(strokeColor));
				tempPolygon.setFill(Color.web(fillColor));
				relGroup.getChildren().add(tempPolygon);
			}
			outGroup.getChildren().add(relGroup);
		}
		return outGroup;
	}*/
	
	
	
	
	public javafx.scene.Group getGroupLand() 
	{		
		Group outGroup = new Group();
		String strokeColor = "#FFFFFF" ; String fillColor = "#FFFFFF";
		boolean land = false;
		for(int i=0; i < this.wayList.size(); i++)
		{
			Polygon tempPolygon = new Polygon();
			OsmWay currWay = this.wayList.get(i);
			// Stroker contour
			// Fill interieur
			if(currWay.hasTag("landuse", "commercial"))
			{
				fillColor = "#E8AABE";
				 strokeColor= "#B4819B"; // 
			} else if(currWay.hasTag("landuse", "construction"))
			{
				fillColor = "#AD9585";
				 strokeColor= "#796658"; // 
			}else if(currWay.hasTag("landuse", "industrial"))
			{
				fillColor = "#E8C2CE";
				 strokeColor= "#CCA3B0"; // 
			}
			else if(currWay.hasTag("landuse", "residenstial"))
			{
				fillColor = "#e5e7e6";
				 strokeColor= "#CFCECE";
			}
			else if(currWay.hasTag("landuse", "farmland"))
			{
				fillColor = "#EEE6D8";
				 strokeColor= "#B9AC98";
			}else if(currWay.hasTag("landuse", "forest"))
			{
				fillColor = "#5D7052";
				 strokeColor= "#465A42";
			}else if(currWay.hasTag("landuse", "basin") || currWay.hasTag("landuse", "salt_pond") || currWay.hasTag("landuse", "reservoir"))
			{
				fillColor = "#5D7052";
				 strokeColor= "#465A42";
			}
			
			
			
			LinkedList<OsmNode> currNodeList = currWay.getNodeList();
			for(int j=0; j < currNodeList.size(); j++)
			{
				Double[] currPoint = this.scalePoint(currNodeList.get(j).getLat(), currNodeList.get(j).getLon());
				tempPolygon.getPoints().addAll(currPoint);
			}
			tempPolygon.setStroke(Color.web(strokeColor));
			tempPolygon.setFill(Color.web(fillColor));
			outGroup.getChildren().add(tempPolygon);
			
		}
		
		return outGroup;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public javafx.scene.Group getGroupRoad()
	{
		javafx.scene.Group outGroup = new Group();
		boolean bool = true;
		for(int i=0; i < this.wayList.size(); i++)
		{
			OsmWay currWay = this.wayList.get(i);
			if(!currWay.hasTag("surface"))
			{
				continue;
			}
			double width = 1.0;
			String color = "#000000";
			if(currWay.hasTag("highway", "primary") || currWay.hasTag("highway", "tertiary"))
			{
				width = 3.0;
				color = "#CE6A6B";
			} else if(currWay.hasTag("highway", "secondary")) {
				width = 3.0;
				color = "#FFBF66";
			} else if(currWay.hasTag("highway", "residential")) {
				width = 1.0;
				color = "#D4D3DC";
			}else if(currWay.hasTag("highway", "service") || currWay.hasTag("highway", "living_street")) {
				width = 1.0;
				color = "#BED3C3";
			} else if(currWay.hasTag("highway", "footway"))
			{
				width = 0.5;
				color = "#BED3C3";
			} else if(currWay.hasTag("highway", "trunk_link") || currWay.hasTag("highway", "trunk")) // trunk
			{
					width = 3.0;
					color = "#CF5C78";
			}else if(currWay.hasTag("cycleway", "lane"))
			{
				width = 1;
				color = "#AD956B";
			} else if(currWay.hasTag("waterway") || currWay.hasTag("waterway", "river") )
			{
				width = 1.0;
				color = "#9AC8EB";
			}else if(currWay.hasTag("highway", "motorway") || currWay.hasTag("highway", "motorway_link"))
			{
				width = 3.0;
				color = "#CE6A6B";
			} 
			
			System.out.println("primary :");
			LinkedList<OsmNode> currNodeList = currWay.getNodeList();
			System.out.println("------------");
			System.out.println("Size of wayList : " + this.getWayList().size());
			
			System.out.println("Size of getNodeList : " + currWay.getNodeList().size());
			System.out.println("Current NodeList: " + currNodeList);
			//Polygon tempPolygon = new Polygon();
			Path road = new Path();
			road.setStrokeWidth(width);
			road.setStroke(Color.web(color));
			MoveTo tempMove = new MoveTo();
			Double[] fisrtCoord = this.scalePoint(currNodeList.get(0).getLat(), currNodeList.get(0).getLon());
			tempMove.setX(fisrtCoord[0]);
			tempMove.setY(fisrtCoord[1]);
			road.getElements().add(tempMove);
			for(int j=1; j < currNodeList.size(); j++)
			{

					Double[] tempCoord = this.scalePoint(currNodeList.get(j).getLat(), currNodeList.get(j).getLon());
					//Polygon tempRect = new Polygon( tempCoord[0], tempCoord[1] , this.getdistance(currNodeList.get(j), currNodeList.get(j+1)), 10);
					//double dist = this.getdistance(currNodeList.get(j), currNodeList.get(j+1));
					//tempPolygon.getPoints().add(tempCoord[0]);
					//tempPolygon.getPoints().add(tempCoord[1]);
					//tempPolygon.getPoints().add(tempCoord[0]+dist);
					//tempPolygon.getPoints().add(tempCoord[1]+dist);
					LineTo tempLine = new LineTo();
					tempLine.setX(tempCoord[0]);
					tempLine.setY(tempCoord[1]);
					
					road.getElements().add(tempLine);
			}
			outGroup.getChildren().add(road);
		}
		
		return outGroup;
	}
	
	/**
	 * Get distance btw 2 node
	 * @param n1
	 * @param n2
	 * @return double
	 */
	public double getdistance(OsmNode n1, OsmNode n2)
	{
		Double[] n1Scale = scalePoint(n1.getLat(), n1.getLon());
		Double[] n2Scale = scalePoint(n1.getLat(), n1.getLon());
		return Math.sqrt(Math.pow((n2Scale[0]-n1Scale[0]), 2) + Math.pow((n2Scale[1]-n1Scale[1]), 2));
	}
	
	/**
	 * Get building for javaFX
	 * @return
	 */
	public javafx.scene.Group getGroupBuilding() 
	{		
		javafx.scene.Group outGroup = new Group();
		String strokeColor = "#6A645A" ; String fillColor = "#F0BE86";
		for(int i=0; i < this.wayList.size(); i++)
		{
			Polygon tempPolygon = new Polygon();
			OsmWay currWay = this.wayList.get(i);
			// Stroker contour
			// Fill interieur
			if(currWay.hasTag("building"))
			{
				System.out.println("\t\t\tBuilding");
				strokeColor = "#6A645A"; 
				fillColor = "#F0BE86";
				
			} else if(currWay.hasTag("natural")) {
				if(currWay.hasTag("natural", "water"))
				{
					System.out.println("\t\t\tWater");
					fillColor = "#9AC8EB";
					 strokeColor= "#5784BA";

				} else if(currWay.hasTag("natural", "scrub") || currWay.hasTag("wetland", "mangrove"))
				{
					System.out.println("\t\t\tScrub");
					 fillColor= "#7AA95C";
					 strokeColor = "#5D7052";
				} else if(currWay.hasTag("natural", "grassland") || currWay.hasTag("wetland", "marsh") || currWay.hasTag("wetland", "reedbed") || currWay.hasTag("wetland", "wed_meadow") || currWay.hasTag("wetland", "fen"))
				{
					fillColor= "#7FA601";
					 strokeColor = "#A4BD01";
				}
				 else if(currWay.hasTag("natural", "heath"))
				{
					fillColor= "#85CB4B";
					 strokeColor = "#A5CB83";
				}
				 else if(currWay.hasTag("natural", "beach") || currWay.hasTag("natural", "sand"))
				{
					fillColor= "#E8D2A1";
					 strokeColor = "#FEEAA1";
				}
				 else if(currWay.hasTag("natural", "wood") || currWay.hasTag("wetland", "swamp"))

				{
					 strokeColor= "#5D7458";
					fillColor = "#5D8D6D";
				}
				 else if(currWay.hasTag("natural", "glacier"))
				{
					 strokeColor= "#EDEDFF";
					fillColor = "#F7F7FF";
				}
				 else if(currWay.hasTag("natural", "shingle"))
					{
						 strokeColor= "#ECE1D7";
						fillColor = "#DACDC4";
					}
			}
			
			LinkedList<OsmNode> currNodeList = currWay.getNodeList();
			for(int j=0; j < currNodeList.size(); j++)
			{
				Double[] currPoint = this.scalePoint(currNodeList.get(j).getLat(), currNodeList.get(j).getLon());
				tempPolygon.getPoints().addAll(currPoint);
			}
			tempPolygon.setStroke(Color.web(strokeColor));
			tempPolygon.setFill(Color.web(fillColor));
			outGroup.getChildren().add(tempPolygon);
			
		}
		
		return outGroup;
	}
	
	/**
	 * Display the map data
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public void showOsmMap() 
	{
        System.out.println("MAP metadata:");
        System.out.println("\t generator : "+this.generator);
        System.out.println("\t version : "+this.apiVersion);
        System.out.println("ALL LIST");
        System.out.println(nodeList);
        System.out.println(relationList);
        System.out.println(wayList);
	}

	/**
	 * Get the info about OpenStreetMap gen
	 * @return generator
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getGenerator() {
		return generator;
	}


	/**
	 * Get OpenStreetMap api version
	 * @return apiVersion
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getApiVersion() {
		return apiVersion;
	}


	/**
	 * Get List of all node
	 * @return LinkedList<OsmNode>
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmNode> getNodeList() {
		return nodeList;
	}


	/**
	 * Get List of all Relation
	 * @return LinkedList<OsmRelation>
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmRelation> getRelationList() {
		return relationList;
	}


	/**
	 * Get List of all way
	 * @return LinkedList<OsmWay>
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public LinkedList<OsmWay> getWayList() {
		return wayList;
	}
	
	
	/**
	 * Scale coordonate for the javaFX window
	 * @param x 
	 * @param y
	 * @return Double[2] = [x', y']
	 */
	private Double[] scalePoint(double x, double y)
	{
		Double[] output = new Double[2];
		output[0] = (x-this.bbox.getFxOrigin().getX())*this.getScaleCoef()*8;
		output[1] = (y-this.bbox.getFxOrigin().getY())*this.getScaleCoef()*8;
		return output;
	}

	
	
}
