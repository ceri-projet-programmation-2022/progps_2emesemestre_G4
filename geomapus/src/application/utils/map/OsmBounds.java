package application.utils.map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import application.utils.GpsTuples;

public class OsmBounds {
	private GpsTuples<Double, Double> minGps;
	private GpsTuples<Double, Double> maxGps;
	
	public OsmBounds(Node n)
	{
		System.out.println("OsmBounds" + ((Element) n).getAttribute("minlat") + " | " + ((Element) n).getAttribute("minlon") );
		this.minGps = new GpsTuples<Double, Double>(Double.parseDouble(((Element) n).getAttribute("minlat")), Double.parseDouble(((Element) n).getAttribute("minlon")));
		this.maxGps = new GpsTuples<Double, Double>(Double.parseDouble(((Element) n).getAttribute("maxlat")), Double.parseDouble(((Element) n).getAttribute("maxlon")));
	}

	public GpsTuples<Double, Double> getMinGps() {
		return minGps;
	}

	public GpsTuples<Double, Double> getMaxGps() {
		return maxGps;
	}
	
	
}
