package application.utils.map;

import org.w3c.dom.Node;
import org.w3c.dom.Element;


/**
 * 
 * @author Aubertin Emmanuel (aka aTHO_)
 * @version 0.1
 * @since 0.1
 * 
 */
public class OsmTag {
	private String type;
	private String value;
	
	/**
	 * Constructor with a tag Node
	 * @param n tag Node of OpenStreetMap XML data file
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public OsmTag(Node n) {
		super();
		this.type = ((Element) n).getAttribute("k");
		this.value = ((Element) n).getAttribute("v");
	}

	/**
	 * Get the type of the tag ("v" in OSM xml)
	 * @return type
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getType() {
		return type;
	}

	/**
	 * Get the value of the tag ("k" in OSM xml)
	 * @return value
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString()
	{
		return "tag => k='" + this.type + "'; v='" + this.value + "'";
	}
}
