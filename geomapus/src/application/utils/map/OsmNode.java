package application.utils.map;



import java.util.LinkedList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * 
 * @author Aubertin Emmanuel (aka aTHO_)
 * @version 0.1
 * @since 0.1
 * 
 */
public class OsmNode extends OsmMember{
	private double lat;
	private double lon;
	private LinkedList<OsmTag> tagList;
	
	/**
	 * Constructor with a node Node
	 * @param n node Node of OpenStreetMap XML data file
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public OsmNode(Node n) {
		super(Long.parseLong(((Element) n).getAttribute("id")), "role", "node"); // Def id and role
		this.lat 		= 	Double.parseDouble(((Element) n).getAttribute("lat"));
		this.lon 		= 	Double.parseDouble(((Element) n).getAttribute("lon"));
		this.tagList 	= 	new LinkedList<OsmTag>();
		if(n.hasChildNodes()) {
			NodeList nodeList = n.getChildNodes();
			for(int i=1;  i < nodeList.getLength(); i++) 
			{
				Node currNode = nodeList.item(i);
				if( currNode.getNodeName() == "tag")
				{
					this.addOsmTag(currNode);
					continue;
				}
			}
		}
	}
	
	
	/**
	 * Constructor with all arg. If you want a OsmNode with no role use OsmNode(long, Node).
	 * @param ref long number also call id
	 * @param role role of the node
	 * @param parentNode ParentNode of all xlm
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public OsmNode(long ref, String role, Node parentNode) {
		super(ref, role, "node"); // Def id and role
		if(parentNode.hasChildNodes()) {
			NodeList parentList = parentNode.getChildNodes();
			for(int i=1;  i < parentList.getLength(); i++) 
			{
				Node currNode = parentList.item(i);
				if( currNode.getNodeName() == "node" && Long.parseLong(((Element) currNode).getAttribute("id")) == ref)
				{
					this.lat 		= 	Float.valueOf(((Element) currNode).getAttribute("lat"));
					this.lon 		= 	Float.valueOf(((Element) currNode).getAttribute("lon"));
					this.tagList 	= new LinkedList<OsmTag>();
					if(currNode.hasChildNodes())
					{
						NodeList nodeList = currNode.getChildNodes();
						for(int j=1;  j < nodeList.getLength(); j++) 
						{
							Node goodNode = nodeList.item(j);
							if( goodNode.getNodeName() == "tag")
							{
								this.addOsmTag(goodNode);
								continue;
							}
						}
					return;
					}
				}
			
			}
		}
	}
	
	/**
	 * Constructor for Node with role
	 * @param ref long number also call id
	 * @param parentNode ParentNode of all xlm
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public OsmNode(long ref, Node parentNode) {
		super(ref, "null", "node"); // Def id and role
		if(parentNode.hasChildNodes()) {
			NodeList parentList = parentNode.getChildNodes();
			for(int i=1;  i < parentList.getLength(); i++) 
			{
				Node currNode = parentList.item(i);
				if( currNode.getNodeName() == "node" && Long.parseLong(((Element) currNode).getAttribute("id")) == ref)
				{
					//System.out.println("its fine");
					this.lat 		= 	Float.valueOf(((Element) currNode).getAttribute("lat"));
					this.lon 		= 	Float.valueOf(((Element) currNode).getAttribute("lon"));
					this.tagList 	= new LinkedList<OsmTag>();
					if(currNode.hasChildNodes())
					{
						NodeList nodeList = currNode.getChildNodes();
						for(int j=1;  j < nodeList.getLength(); j++) 
						{
							Node goodNode = nodeList.item(j);
							if( goodNode.getNodeName() == "tag")
							{
								this.addOsmTag(goodNode);
								continue;
							}
						}
					return;
					}
				}
			}
		}
	}
	
	/**
	 * Know if the Node get a particular tag
	 * @param value put the 'v' field
	 * @param key put the 'k' field
	 * @return true if the node has the tag
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public boolean hasTag(String value, String key) 
	{
		for (int i = 0; i < this.tagList.size(); i++)
		{
			if(this.tagList.get(i).getType() == key && this.tagList.get(i).getValue() == value)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Get the lat of the member
	 * @return lat
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public double getLat() {
		return lat;
	}

	/**
	 * Get the lon of the member
	 * @return lon
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public double getLon() {
		return lon;
	}
	
	/**
	 * Add tag to the node
	 * @param n OSM tag Node
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
 	 */
	public void addOsmTag(Node n)
	{
		this.tagList.push(new OsmTag(n));
	}
	
	/**
	 * Get the OsmNode in a good string
	 * @return OsmNode String
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String toString() {
		return Long.toString(this.id);
	}
	
}