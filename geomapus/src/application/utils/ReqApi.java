package application.utils;

import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.io.*;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;


/**
 * 
 * @author Aubertin Emmanuel (aka aTHO_)
 * @version 0.1
 * @since 0.1
 */
public class ReqApi {
	private static enum HttpType { GET, POST };
	private static enum AuthType { NONE, BEARER_TOKEN };
	private static String defaultHttpType="GET";
	private static String defaultAuthType="NONE";

	private URL urlReq;
	private boolean status;
	private HttpType reqHttpType;
	private AuthType reqAuthType;
	private HttpURLConnection httpCon;
	
	

	/**
	 * Constructor with every argument
	 * @param inUrl
	 * @param inHttpType
	 * @param inAuthType
	 * @throws IOException
	 *  
	 *  @author Aubertin Emmanuel (aka aTHO_)
	 */
	public ReqApi(String inUrl, String inHttpType, String inAuthType) throws IOException {
		urlReq = new URL(inUrl);
		httpCon = (HttpURLConnection) urlReq.openConnection(); 
		setAuthType(inAuthType);
		setHttpType(inHttpType);
		status = true;
	}
	
	/**
	 * Minimal constructor
	 * @param inUrl
	 * @throws ProtocolException
	 * @throws MalformedURLException 
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public ReqApi(String inUrl) throws IOException {
		urlReq = new URL(inUrl);
		httpCon = (HttpURLConnection) urlReq.openConnection(); 
		setAuthType(defaultAuthType);
		setHttpType(defaultHttpType);
		status = true;
	}	
	
	/**
	 * Send and return result http request
	 * @return String with the http return
	 * @throws IOException 
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 **/

	public String getStrFromReq() throws IOException
	{
		BufferedReader inputStr = new BufferedReader( new InputStreamReader(httpCon.getInputStream()));
		String line;
		StringBuffer outContent = new StringBuffer();
		while ((line = inputStr.readLine()) != null)
		{
			outContent.append(line);
		}
		return outContent.toString();
	}
	
	/**
	 * 
	 * @return Document parsed 
	 * @throws Exception
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
    public Document getXmlFromReq() throws Exception
    {
    	System.out.println("Inside getXmlFromReq()");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        System.out.println("Factory is build");
        DocumentBuilder builder = factory.newDocumentBuilder();
        System.out.println("Send and parse");
        return builder.parse(httpCon.getInputStream());
    }
    
	/**
	 * Update the timeout value
	 * @param inTimeMs time in ms
	 * @return true if timeout has been set
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public boolean setTimeout(int inTimeMs)
	{
		try {
			httpCon.setConnectTimeout(inTimeMs);
			return true;
		} catch (Exception e) {
	        return false;
	    }
	}
	
	/**
	 * Get the timeout in ns
	 * @return int timeout in ms
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public int getTimeout() {
		return httpCon.getConnectTimeout();
	}
	
	/**
	 * Set property in HTTP Header
	 * @param inKey
	 * @param inValue
	 * @return true if it works
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public boolean setProperty(String inKey, String inValue) {
		try {
			httpCon.setRequestProperty(inKey, inValue);
			return true;
		} catch (Exception e) {
	        return false;
	    }
	}
	
	/**
	 * Get the name of the Auth type
	 * @return Auth type
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getAuthType() {
		return reqAuthType.name();
	}
	
	/**
	 * Update the Auth value
	 * @param inAuth value of auth field
	 * @return true if type is set
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public boolean setAuth(String inAuth) {
		if(reqAuthType == AuthType.NONE) {
			return true;
		}
		
		if(reqAuthType == AuthType.BEARER_TOKEN) {
			httpCon.setRequestProperty("Authorization", "Bearer "+inAuth);
			return true;
		}
		return false;
	}
	
	/**
	 * Update the Auth type
	 * @param inType type of the Auth
	 * @return true if type is set
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public boolean setAuthType(String inType) {
		if(inType.toUpperCase() == "NONE") {
			reqAuthType = AuthType.NONE;
			return true;
		}
		if(inType.toUpperCase() == "BEARER") {
			reqAuthType = AuthType.BEARER_TOKEN;
			return true;
		}
		return false;
	}
	
	/**
	 * Get url
	 * @return Url of the object
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getUrl() {
	    return urlReq.toExternalForm();
	}
	
	/**
	 * Get name of the HTTP type
	 * @return Http type
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public String getHttpType() {
		return reqHttpType.name();
	}
	
	/**
	 * 
	 * @param inType type of the HTTP req ("GET" or "POST")
	 * @return true if type is set
	 * @throws ProtocolException 
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public boolean setHttpType(String inType) throws ProtocolException {
		if(inType.toUpperCase() == "GET") {
			reqHttpType = HttpType.GET;
			httpCon.setRequestMethod("GET");
			return true;
		}
		
		if(inType.toUpperCase() == "POST") {
			reqHttpType = HttpType.POST;
			httpCon.setRequestMethod("POST");
			return true;
		}
		
		return false;
	}
	
	/**
	 * Check statues of the req
	 * @return true if req is closed
	 * 
	 * @author Aubertin Emmanuel (aka aTHO_)
	 */
	public boolean isClosed() {
		return status;
	}

}