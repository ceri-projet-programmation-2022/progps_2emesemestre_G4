package application;


import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;

import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.w3c.dom.Document;

import application.utils.ReqApi;
import application.utils.map.OsmBbox;
import application.utils.map.OsmMap;
import application.utils.map.OsmRelation;
import application.utils.map.OsmWay;
import javafx.event.ActionEvent;

import javafx.application.Platform;

import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;


public class VueController {

	 /* Yassmine */
	 @FXML private TextField shearchField;
	 
	
	 

	 @FXML private Button btnAffiche;
	 @FXML private AnchorPane map;
	 
	 @FXML private ExecutorService executor  = Executors.newSingleThreadExecutor();
	 // Alert en cas d'erreur dans la saisie du nom de la ville 
	 private void showAlertWithHeaderText() {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning alert");
			alert.setHeaderText("Controle Text saisie :");
			alert.setContentText("Le nom d'une ville ne peut pas contenir des caractéres spéciaux");
			alert.showAndWait();
	}
	 
	 
	 @FXML
	 void afficherIteneraire(ActionEvent event) {
	     String nomVille = shearchField.getText();
	     char[] charSpecialListe = {'y','e','w'}; 
	     for(int i=0; i<nomVille.length(); i++) 
	        {
	            char chr = nomVille.charAt(i);
	            for(int j=0; j<charSpecialListe.length; j++)
	            {
	                if(charSpecialListe[j] == chr)
	                {
	           	     	System.out.println("le nom saisie contient un caractere spécial");

	                	showAlertWithHeaderText(); 
	                }
	            }  
	        }
	     
	 }
	 
	
	    
	    /**
	     * Event handler of the button "Afficher"
	     * @param event
	     * @throws IOException
	     * @throws InterruptedException
	     * 
	     * @author Aubertin Emmanuel (aka aTHO_)
	     * @throws TimeoutException 
	     */
	    public void showMap(ActionEvent event) throws IOException, InterruptedException, TimeoutException {

	    	map.getChildren().clear();
	        System.out.println( "--------------------------------------------------------------" );
	        // Async func  
	        double mapHeight = map.getHeight();
	       double mapWidth = map.getWidth();
	       double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	       x1 =   3.7869650307486893;
	   		y1 =  11.470584869384766;
	   		
	   		x2 = 3.8430167329671794;
	   		y2 = 11.54263973236084;
	   		
	   		OsmBbox bbox = new OsmBbox(x1, y1, x2, y2);
	   		
	   		if(shearchField.getText() != "")
	   		{
	   			while(bbox.getOsmOrigin().getX() == x1 || bbox.getOsmOrigin().getX() == x2)
	   			{
		   			Future<OsmBbox> isRoadPrim = getBbox(shearchField.getText());
		   			try {
						bbox = isRoadPrim.get(8, TimeUnit.SECONDS);
					} catch (Exception e) {
						System.out.print("Ah");
					}
	   			}
	   		}
   		
	   		double bboxDist = OsmWay.getdistance(bbox.getOsmOrigin(), bbox.getOsmOposite());
	   		System.out.println("------");
	   		System.out.println("\tBbox size = "+ bboxDist);
	   		System.out.println("\tSize ref =  0.0456445291570693");
	   		System.out.println("------");
	   		// 3.8016099073914815,11.490240097045898,3.8296357387323705,11.526267528533936
	   		if( bboxDist < 0.0456445291570693)
	    	{
	   			Future<Boolean> isRoadPrim = getRoad(event,"[\"highway\"=\"primary\"][\"highway\"=\"primary_link\"]", mapHeight, mapWidth, bbox); // Work in progress
	    		Future<Boolean> isRoadSec = getRoad(event,"[\"highway\"=\"secondary\"][\"highway\"=\"secondary_link\"]", mapHeight, mapWidth, bbox); // Work in progress
	    		Future<Boolean> isRoadTer = getRoad(event,"[\"highway\"=\"tertiary\"][\"highway\"=\"tertiary_link\"]", mapHeight, mapWidth, bbox); // Work in progress
	    		Future<Boolean> isRoadLMot = getRoad(event,"[\"highway\"=\"motorway\"][\"highway\"=\"motorway_link\"]", mapHeight, mapWidth, bbox); // Work in progress
	    		Future<Boolean> isRoadLTrunk = getRoad(event,"[\"highway\"=\"trunk\"][\"highway\"=\"trunk_link\"]", mapHeight, mapWidth, bbox); // Work in progress  
	    		Future<Boolean> isRoadLCycle = getRoad(event,"[\"cycleway\"]", mapHeight, mapWidth, bbox);
	    		
	    		Future<Boolean> isNatural = getBuilding(event, "[\"natural\"]", mapHeight, mapWidth, bbox);
	    		
	    		// Enconmy
	    		Future<Boolean> isBuildingCom = getBuilding(event, "[\"building\"=\"commercial\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingIndus = getBuilding(event,"[\"building\"=\"industrial\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingKiosk = getBuilding(event, "[\"building\"=\"kiosk\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingOffice = getBuilding(event, "[\"building\"=\"office\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingRetail = getBuilding(event, "[\"building\"=\"retail\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingMarket = getBuilding(event, "[\"building\"=\"supermarket\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingWarehouse = getBuilding(event, "[\"building\"=\"warehouse\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingReligious = getBuilding(event, "[\"building\"=\"religious\"]", mapHeight, mapWidth, bbox);
	    		//Habitation
	    		Future<Boolean> isBuildingHouse = getBuilding(event, "[\"building\"=\"house\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingBarracks = getBuilding(event, "[\"building\"=\"barracks\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingBungalow = getBuilding(event, "[\"building\"=\"bungalow\"][\"building\"=\"dormitory\"][\"building\"=\"ger\"][\"building\"=\"static_caravan\"][\"building\"=\"cabin\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingFarm = getBuilding(event, "[\"building\"=\"farm\"][\"building\"=\"farm\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingHouseSpe = getBuilding(event, "[\"building\"=\"stilt_house\"][\"building\"=\"tree_house\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingHotel = getBuilding(event, "[\"building\"=\"toilet\"][\"building\"=\"hotel\"]", mapHeight, mapWidth, bbox);
	    		//public
	    		Future<Boolean> isBuildingUniv = getBuilding(event, "[\"building\"=\"university\"][\"building\"=\"college\"][\"building\"=\"kindergarten\"][\"building\"=\"school\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingTrain = getBuilding(event,"[\"building\"=\"train_station\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingTrans = getBuilding(event, "[\"building\"=\"transportation\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingMedecin = getBuilding(event, "[\"building\"=\"hospital\"][\"building\"=\"fire_station\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingGouv = getBuilding(event, "[\"building\"=\"government\"][\"building\"=\"military\"][\"building\"=\"service\"][\"building\"=\"digester\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingEnergyStockage = getBuilding(event, "[\"building\"=\"transformer_tower\"][\"building\"=\"water_tower\"][\"building\"=\"storage_tank\"][\"building\"=\"silo\"]", mapHeight, mapWidth, bbox);
	    		//Agriculture
	    		Future<Boolean> isBuildingFarming = getBuilding(event, "[\"building\"=\"farm_auxiliary\"][\"building\"=\"conservatory\"][\"building\"=\"barn\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingAnimals = getBuilding(event, "[\"building\"=\"cowshed\"][\"building\"=\"slurry_tank\"][\"building\"=\"stable\"][\"building\"=\"sty\"]", mapHeight, mapWidth, bbox);
	    		//Stockage
	    		Future<Boolean> isBuildingStockage = getBuilding(event, "[\"building\"=\"hangar\"][\"building\"=\"hut\"][\"building\"=\"shed\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingAppart = getBuilding(event, "[\"building\"=\"apartments\"]", mapHeight, mapWidth, bbox); // Possiblement beaucoup de data
	    		
	    		// Possiblement beaucoup de data
	    		Future<Boolean> isRoadLRes = getRoad(event,"[\"highway\"=\"residential\"]", mapHeight, mapWidth, bbox); 

	    	} else {
	    		System.out.println("Grande BBOX = Pas de building");
	    		//Future<Boolean> isLandRes = getLand(event, "[\"landuse\"=\"residential\"]", mapHeight, mapWidth, bbox);
	    		
	    		Future<Boolean> isLandCons = getLand(event, "[\"landuse\"=\"construction\"]", mapHeight, mapWidth, bbox);
	
				
	    		Future<Boolean> isLandIndus = getLand(event, "[\"landuse\"=\"industrial\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isLandCom = getLand(event, "[\"landuse\"=\"commercial\"]", mapHeight, mapWidth, bbox);
	    		
	    		
	    		Future<Boolean> isLandFores = getLand(event, "[\"landuse\"=\"forest\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isLandWater = getLand(event, "[\"landuse\"=\"basin\"][\"landuse\"=\"salt_pond\"][\"landuse\"=\"reservoir\"]", mapHeight, mapWidth, bbox);
	    		
	    		
	    		
	    		Future<Boolean> isNatural = getBuilding(event, "[\"natural\"=\"grassland\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
	    		Future<Boolean> isNaturalWater = getBuilding(event, "[\"natural\"=\"wetland\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
	    		
	    		Future<Boolean> isWaterlagoon = getBuilding(event, "[\"water\"=\"lagoon\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
	    		Future<Boolean> isWaterlake = getBuilding(event, "[\"water\"=\"lake\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
	    		Future<Boolean> isWaterOxbow = getBuilding(event, "[\"water\"=\"oxbow\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
	    		Future<Boolean> isWaterRiver = getBuilding(event, "[\"water\"=\"river\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
	    		Future<Boolean> isWaterRapids= getBuilding(event, "[\"water\"=\"rapids\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
	    		Future<Boolean> isWaterStream = getBuilding(event, "[\"water\"=\"stream\"][\"water\"=\"stream_pool\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
	    		
	    		
				Future<Boolean> isNaturalScrub = getBuilding(event, "[\"natural\"=\"scrub\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
				Future<Boolean> isNaturalHeath = getBuilding(event, "[\"natural\"=\"heath\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
				Future<Boolean> isNaturalBeachSand = getBuilding(event, "[\"natural\"=\"beach\"][\"natural\"=\"sand\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
				Future<Boolean> isNaturalWood = getBuilding(event, "[\"natural\"=\"wood\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
				Future<Boolean> isNaturalGlacier = getBuilding(event, "[\"natural\"=\"glacier\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine
				Future<Boolean> isNaturalShingle = getBuilding(event, "[\"natural\"=\"shingle\"]", mapHeight, mapWidth, bbox); // natural viens Override le landuse mais est sous tout infrastrucre humaine

	    		
	    		
	    		
	    		Future<Boolean> isRoadPrim = getRoad(event,"[\"highway\"=\"primary\"]", mapHeight, mapWidth, bbox); // Work in progress
	    		Future<Boolean> isRoadSec = getRoad(event,"[\"highway\"=\"secondary\"]", mapHeight, mapWidth, bbox); // Work in progress
	    		Future<Boolean> isRoadTer = getRoad(event,"[\"highway\"=\"tertiary\"]", mapHeight, mapWidth, bbox); // Work in progress
	    		Future<Boolean> isRoadLMot = getRoad(event,"[\"highway\"=\"motorway\"]", mapHeight, mapWidth, bbox); // Work in progress
	    		Future<Boolean> isRoadLTrunk = getRoad(event,"[\"highway\"=\"trunk\"]", mapHeight, mapWidth, bbox); // Work in progress  
	    		Future<Boolean> isRoadLCycle = getRoad(event,"[\"cycleway\"]", mapHeight, mapWidth, bbox);
	    		
	    		
	    		
	    		
	    	
	    		// Enconmy
	    		Future<Boolean> isBuildingCom = getBuilding(event, "[\"building\"=\"commercial\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingIndus = getBuilding(event,"[\"building\"=\"industrial\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingKiosk = getBuilding(event, "[\"building\"=\"kiosk\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingOffice = getBuilding(event, "[\"building\"=\"office\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingRetail = getBuilding(event, "[\"building\"=\"retail\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingMarket = getBuilding(event, "[\"building\"=\"supermarket\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingWarehouse = getBuilding(event, "[\"building\"=\"warehouse\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingReligious = getBuilding(event, "[\"building\"=\"religious\"]", mapHeight, mapWidth, bbox);
	    		
	    		
	    		//Habitation
	    		Future<Boolean> isBuildingHouse = getBuilding(event, "[\"building\"=\"house\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingBarracks = getBuilding(event, "[\"building\"=\"barracks\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingBungalow = getBuilding(event, "[\"building\"=\"bungalow\"][\"building\"=\"dormitory\"][\"building\"=\"ger\"][\"building\"=\"static_caravan\"][\"building\"=\"cabin\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingFarm = getBuilding(event, "[\"building\"=\"farm\"][\"building\"=\"farm\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingHouseSpe = getBuilding(event, "[\"building\"=\"stilt_house\"][\"building\"=\"tree_house\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingHotel = getBuilding(event, "[\"building\"=\"toilet\"][\"building\"=\"hotel\"]", mapHeight, mapWidth, bbox);
	    		//public
	    		Future<Boolean> isBuildingUniv = getBuilding(event, "[\"building\"=\"university\"][\"building\"=\"college\"][\"building\"=\"kindergarten\"][\"building\"=\"school\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingTrain = getBuilding(event,"[\"building\"=\"train_station\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingTrans = getBuilding(event, "[\"building\"=\"transportation\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingMedecin = getBuilding(event, "[\"building\"=\"hospital\"][\"building\"=\"fire_station\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingGouv = getBuilding(event, "[\"building\"=\"government\"][\"building\"=\"military\"][\"building\"=\"service\"][\"building\"=\"digester\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingEnergyStockage = getBuilding(event, "[\"building\"=\"transformer_tower\"][\"building\"=\"water_tower\"][\"building\"=\"storage_tank\"][\"building\"=\"silo\"]", mapHeight, mapWidth, bbox);
	    		//Agriculture
	    		Future<Boolean> isBuildingFarming = getBuilding(event, "[\"building\"=\"farm_auxiliary\"][\"building\"=\"conservatory\"][\"building\"=\"barn\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingAnimals = getBuilding(event, "[\"building\"=\"cowshed\"][\"building\"=\"slurry_tank\"][\"building\"=\"stable\"][\"building\"=\"sty\"]", mapHeight, mapWidth, bbox);
	    		//Stockage
	    		Future<Boolean> isBuildingStockage = getBuilding(event, "[\"building\"=\"hangar\"][\"building\"=\"hut\"][\"building\"=\"shed\"]", mapHeight, mapWidth, bbox);
	    		Future<Boolean> isBuildingAppart = getBuilding(event, "[\"building\"=\"apartments\"]", mapHeight, mapWidth, bbox); // Possiblement beaucoup de data
	    		
	    		
	    		Future<Boolean> isRoadLRes = getRoad(event,"[\"highway\"=\"residential\"]", mapHeight, mapWidth, bbox); // Possiblement beaucoup de data
	    		
	  
	    		
	    	}
	    	
	    	
	    	
       }
	    
	    /**
	     * Test of drawing a city.
	     * @param event
	     * @param my_city value on cityInput
	     * @return true
	     * @throws IOException
	     * @throws InterruptedException
	     * 
	     * @author Aubertin Emmanuel (aka aTHO_)
	     */
	    public Future<Boolean> getBuilding(ActionEvent event, String data, double mapHeight, double mapWidth, OsmBbox bbox) throws IOException, InterruptedException  {
	    	return executor.submit(() -> {
	    		System.out.println( "asking for the XML" );
	    		
	    		
	    		//String urlReq = "https://overpass-api.de/api/interpreter?relation[name=\""+ my_city +"\"][type=boundary][admin_level=8];(._;>;);out;";
		    	String urlReq = "http://13.39.84.120/api/interpreter?data=";
		    	System.out.println( "Build data urlencoded" );
		    	String urlBuildingData	= "[out:xml][timeout:25];(way"+ data + bbox.getStrOsm() + ";relation"+ data + bbox.getStrOsm() +";);out body;>;out skel qt;";
		    	
		    	System.out.println( urlReq +  urlBuildingData);
		    	urlBuildingData = java.net.URLEncoder.encode(urlBuildingData, "UTF-8");
		    	System.out.println( urlReq +  urlBuildingData);
		    	
		        ReqApi buildingReq = new ReqApi( urlReq + urlBuildingData);
		        
		        Document buildinDocument =  buildingReq.getXmlFromReq();
		        
		        System.out.println("Parsing in Obj");
		        
		        OsmMap buildMap = new OsmMap(buildinDocument.getFirstChild());
		        
		        buildMap.setBbox(bbox);
		        buildMap.setScaleVect( mapHeight, mapWidth);
		        
		        javafx.scene.Group building = buildMap.getGroupBuilding();
		        
		  
		        
		        Platform.runLater(new Runnable() {
		    	    @Override
		    	    public void run() {
		    	    	building.getTransforms().add(new Rotate(270,0,0)); // Mettre dans l'orientation de la fenetre
		    	        map.getChildren().add(building);
		    	    }
		    	});
		       
		        
		        System.out.println(bbox);

		        return 	true;
	    	
	    	});

			
	    }
	    
	    
	    /**
	     * Test of drawing a city.
	     * @param event
	     * @param my_city value on cityInput
	     * @return true
	     * @throws IOException
	     * @throws InterruptedException
	     * 
	     * @author Aubertin Emmanuel (aka aTHO_)
	     */
	    public Future<OsmBbox> getBbox(String data) throws IOException, InterruptedException  {
	    	return executor.submit(() -> {
	    		System.out.println( "asking for the XML" );
	    		
	    		
	    		//String urlReq = "https://overpass-api.de/api/interpreter?relation[name=\""+ my_city +"\"][type=boundary][admin_level=8];(._;>;);out;";
		    	String urlReq = "https://overpass.kumi.systems/api/interpreter?data=";
		    	System.out.println( "Build data urlencoded" );
		    	String urlBuildingData	= "[out:xml][timeout:25];area[\"name\"=\"France\"]->.a;(relation(area.a)[\"boundary\"=\"administrative\"][\"name\"=\""+ data +"\"];);out tags bb;";
	    	
		    	System.out.println( urlReq +  urlBuildingData);
		    	urlBuildingData = java.net.URLEncoder.encode(urlBuildingData, "UTF-8");
		    	System.out.println( urlReq +  urlBuildingData);
		    	
		        ReqApi buildingReq = new ReqApi( urlReq + urlBuildingData);
		        
		        Document buildinDocument =  buildingReq.getXmlFromReq();
		        
		        System.out.println("Parsing in Obj");
		        
		        OsmMap buildMap = new OsmMap(buildinDocument.getFirstChild());
		        
		        LinkedList<OsmRelation> relList = buildMap.getRelationList();
		        
		        double minlat = 999.9, minlon = 999.9, maxlat = -999.9, maxlon = -999.9;
		        
		        for(int i=0; i < relList.size(); i++)
		        {
		        	if( relList.get(i).getBounds().getMinGps().getX() < minlat && relList.get(i).getBounds().getMinGps().getY() < minlon)
		        	{
		        		minlat = relList.get(i).getBounds().getMinGps().getX();
		        		minlon = relList.get(i).getBounds().getMinGps().getY();
		        	}
		        	if( relList.get(i).getBounds().getMaxGps().getX() > maxlat && relList.get(i).getBounds().getMaxGps().getY() > maxlon)
		        	{
		        		maxlat = relList.get(i).getBounds().getMaxGps().getX();
		        		maxlon = relList.get(i).getBounds().getMaxGps().getY();
		        	}
		        }
		  
		        	       
		        return 	 new OsmBbox(minlat, minlon, maxlat, maxlon);
	    	
	    	});

			
	    }
	    
	    
	    public Future<Boolean> getRoad(ActionEvent event, String data, double mapHeight, double mapWidth, OsmBbox bbox) throws IOException, InterruptedException  {
	    	return executor.submit(() -> {
	    		System.out.println( "asking for the XML" );

	    		
	    		//String urlReq = "https://overpass-api.de/api/interpreter?relation[name=\""+ my_city +"\"][type=boundary][admin_level=8];(._;>;);out;";
		    	String urlReq = "http://13.39.84.120/api/interpreter?data=";
		    	System.out.println( "Build data urlencoded" );
		    	String urlHighwayData	= "[out:xml][timeout:25];(way"+ data + bbox.getStrOsm() + ";relation"+ data + bbox.getStrOsm() +";);out body;>;out skel qt;";
		    	
		    	
					
		    	System.out.println( urlReq +  urlHighwayData);
		    	urlHighwayData = java.net.URLEncoder.encode(urlHighwayData, "UTF-8");
		    	System.out.println( urlReq +  urlHighwayData);
		    	
		        ReqApi higwayReq = new ReqApi( urlReq + urlHighwayData);
		        
		        Document higwayDocument =  higwayReq.getXmlFromReq();
		        
		        System.out.println("Parsing in Obj");
		        
		        OsmMap higwayMap = new OsmMap(higwayDocument.getFirstChild());
		        
		        higwayMap.setBbox(bbox);
		        higwayMap.setScaleVect(mapHeight, mapWidth);
		        //buildMap.setZoom(10);
		        
		        Group road = higwayMap.getGroupRoad();
		        
		        Platform.runLater(new Runnable() {
		    	    @Override
		    	    public void run() {
		    	    	road.getTransforms().add(new Rotate(270,0,0)); // Mettre dans l'orientation de la fenetre
		    	        map.getChildren().add(road);
		    	    }
		    	});
		       

		        return 	true;
	    	
	    	});

			
	    }
	    
	    
	    
	    /**
	     * Test of drawing a city.
	     * @param event
	     * @param my_city value on cityInput
	     * @return true
	     * @throws IOException
	     * @throws InterruptedException
	     * 
	     * @author Aubertin Emmanuel (aka aTHO_)
	     */
	    public Future<Boolean> getLand(ActionEvent event, String data, double mapHeight, double mapWidth, OsmBbox bbox) throws IOException, InterruptedException  {
	    	return executor.submit(() -> {
	    		System.out.println( "asking for the XML" );
	    		
	    		//String urlReq = "https://overpass-api.de/api/interpreter?relation[name=\""+ my_city +"\"][type=boundary][admin_level=8];(._;>;);out;";
		    	String urlReq = "http://13.39.84.120/api/interpreter?data=";
		    	System.out.println( "Build data urlencoded" );
		    	String urlBuildingData	= "[out:xml][timeout:25];(way"+ data + bbox.getStrOsm() + ";relation"+ data + bbox.getStrOsm() +";);out body;>;out skel qt;";
		    	
		    	System.out.println( urlReq +  urlBuildingData);
		    	urlBuildingData = java.net.URLEncoder.encode(urlBuildingData, "UTF-8");
		    	System.out.println( urlReq +  urlBuildingData);
		    	
		        ReqApi buildingReq = new ReqApi( urlReq + urlBuildingData);
		        
		        Document buildinDocument =  buildingReq.getXmlFromReq();
		        if( buildinDocument.getFirstChild() == null) {
		        	getLand(event,  data,  mapHeight,  mapWidth,  bbox);
		        }
		        System.out.println("Parsing in Obj");
		        
		        OsmMap buildMap = new OsmMap(buildinDocument.getFirstChild());
		        System.out.println("Parsed");
		        
		        System.out.println("Set Scale && bbox");
		        buildMap.setBbox(bbox);
		        System.out.println("buildMap.setScaleVect( " + mapHeight + ", "+ mapWidth + ");");
		        buildMap.setScaleVect( mapHeight, mapWidth);
		        System.out.println("OK Scale && bbox");
		        
		        System.out.println("Get group");
		        Group landuse = buildMap.getGroupLand();
		        System.out.println("Group OK");
		  
		        
		        Platform.runLater(new Runnable() {
		    	    @Override
		    	    public void run() {
		    	    	 System.out.println("rotate");
		    	    	landuse.getTransforms().add(new Rotate(270,0,0)); // Mettre dans l'orientation de la fenetre
		    	    	System.out.println("Add landuse");
		    	        map.getChildren().add(landuse);
		    	        System.out.println("Kk");
		    	    }
		    	});
		       
		        
		        System.out.println(bbox);

		        return 	true;
	    	
	    	});

			
	    }
	    
	       
}
