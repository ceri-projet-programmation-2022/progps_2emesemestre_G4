package application;
	
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.Future;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.*;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

import application.utils.ReqApi;
import application.utils.map.OsmMap;
import application.utils.map.OsmMember;
import application.utils.map.OsmNode;
import application.utils.map.OsmRelation;
import application.utils.map.OsmWay;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.fxml.*;



public class Main extends Application {
	
	//public Scene MainScene;
	
	@Override
	public void start(Stage primaryStage) {


			try {
				Parent parent =  (Parent) FXMLLoader.load(getClass().getResource("Vue.fxml"));
				
				
				Scene MainScene = new Scene(parent);				
				primaryStage.setScene(MainScene);
				primaryStage.setTitle("Geomapus");
				primaryStage.show();
				
				

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	         
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	

}
