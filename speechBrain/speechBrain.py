from asyncio import sleep
import speech_recognition as sr

from speechbrain.pretrained import SpeakerRecognition
import torchaudio

import os

os.system("rm *.wav")

testeur = SpeakerRecognition.from_hparams(source="speechbrain/spkrec-ecapa-voxceleb", savedir="pretrained_models/spkrec-ecapa-voxceleb")

recognizer = sr.Recognizer()

with sr.Microphone() as source:
	print("Gardez le silence pour un moment svp !")
	recognizer.adjust_for_ambient_noise(source=source, duration=10)
	print("Parler...")
	try:
		enregistrement = recognizer.listen(source)

		fichier = open("./tempo.wav", "wb")
		fichier.write(enregistrement.get_wav_data())
		fichier.close()

		authentifier = ["./personne_enregistree/yasmine.wav","./personne_enregistree/kenza.wav"]
		auth = False
		for x in authentifier :
	
			score, prediction = testeur.verify_files("./tempo.wav", x)
			#print("test pour " , x) debug
			if(prediction):
				auth = True
				break
		
		if(auth):
			print("Votre commande vas etre traitee")
			text = recognizer.recognize_google(enregistrement, language="fr-FR")
			print("Vous avez dit : " + text)
		else:
			print("Nous n'avons pas pu vous identifier, veuillez reessayer plus tard.")
		
		

	except:
		print("erreur")

